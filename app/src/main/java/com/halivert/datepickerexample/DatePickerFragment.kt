package com.halivert.datepickerexample

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment

class DatePickerFragment(
    private val listener: DatePickerDialog.OnDateSetListener,
    private val year: Int,
    private val month: Int,
    private val day: Int
) : DialogFragment(), DatePickerDialog.OnDateSetListener {
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        listener.onDateSet(view, year, month, dayOfMonth)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        Log.e("day", day.toString())
        Log.e("month", month.toString())
        Log.e("year", year.toString())
        return DatePickerDialog(activity, this, year, month, day)
    }
}