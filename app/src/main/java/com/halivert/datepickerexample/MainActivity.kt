package com.halivert.datepickerexample

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val editText = findViewById<EditText>(R.id.editText)

        editText.setOnClickListener {
            val c = Calendar.getInstance()
            var day = c.get(Calendar.DAY_OF_MONTH)
            var month = c.get(Calendar.MONTH)
            var year = c.get(Calendar.YEAR)

            if (editText.text.split('-').size == 3) {
                day = editText.text.split('-')[0].toInt()
                month = editText.text.split('-')[1].toInt() - 1
                year = editText.text.split('-')[2].toInt()
            }

            val datePickerFragment =
                DatePickerFragment(DatePickerDialog.OnDateSetListener { _, rYear, rMonth, dayOfMonth ->
                    val dayString = dayOfMonth.toString().padStart(2, '0')
                    val monthString = (rMonth + 1).toString().padStart(2, '0')
                    findViewById<EditText>(R.id.editText).setText("${dayString}-${monthString}-$rYear")
                }, year, month, day)

            datePickerFragment.show(supportFragmentManager, "datePicker")
        }
    }
}
